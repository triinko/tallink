package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookingSummary {

    private final WebDriver driver;

    By adultPassengersCountLocator = By.cssSelector("a.passengersCount > span.adults > span.count");
    By underagePassengersCountLocator = By.cssSelector("a.passengersCount > span.underages > span.count");
    By tripDetailLocator = By.cssSelector("section.tripDetail > h3");
    By selectedDepartureTimeLocator = By.xpath("//div[@class='selectedSail']/time");
    By fullTripDetailLocator = By.cssSelector("section.tripDetail");
    By landServicesLocator = By.className("landServices");
    By hotelDetailLocator = By.cssSelector("section.hotel");
    By tripPriceLocator = By.cssSelector("div.priceContainer > span.totalPrice > span.amount");

    public BookingSummary(WebDriver driver) {
        this.driver = driver;
    }

    public int adultPassengersCount() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(adultPassengersCountLocator));
        return Integer.parseInt(driver.findElement(adultPassengersCountLocator).getText());
    }

    public int underagePassengersCount() {
        WebElement webElement = driver.findElement(underagePassengersCountLocator);
        return Integer.parseInt(webElement.getText());
    }

    public String tripDepartureLocation() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(adultPassengersCountLocator));
        WebElement tripDetails = driver.findElement(tripDetailLocator);
        return tripDetails.getText().split(" ")[0];
    }

    public String tripDetail() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(adultPassengersCountLocator));
        WebElement tripDetails = driver.findElement(tripDetailLocator);
        return tripDetails.getText();
    }

    public String selectedSailTime() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(selectedDepartureTimeLocator));
        WebElement webElement = driver.findElement(selectedDepartureTimeLocator);
        return webElement.getAttribute("datetime");
    }

	public String fullTripDetail() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(fullTripDetailLocator));
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        WebElement tripDetails = driver.findElement(fullTripDetailLocator);
        return tripDetails.getText();
	}

	public boolean hasLandServices() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(landServicesLocator));
        WebElement webElement = driver.findElement(landServicesLocator);
        return webElement.isDisplayed();
    }

	public String hotelTripDetail() {
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(hotelDetailLocator));
        WebElement tripDetails = driver.findElement(hotelDetailLocator);
        return tripDetails.getText();
	}
	
	public int tripPriceAmount() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        WebElement webElement = driver.findElement(tripPriceLocator);
        return Integer.parseInt(webElement.getText());
    }
}
