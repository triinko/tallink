package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class TimeTable {

    private WebDriver driver;

    By shuttleCalendarLocator = By.cssSelector("div[class='timetable shuttle']");
    By dayCruiseCalendarLocator = By.className("dayCruise");
    By cruiseCalendarLocator = By.className("lowFareCalendar");
    By routeCalendarLocator = By.cssSelector("div[class='timetable routetrip']");
    By selectedDateLocator = By.cssSelector("section[class='daySchedule  selectedDate']");

    public TimeTable(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isShuttleCalendarDisplayed() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(shuttleCalendarLocator));
        WebElement shuttleCalendar = driver.findElement(shuttleCalendarLocator);
        return shuttleCalendar.isDisplayed();
    }

    public boolean isDayCruiseCalendarDisplayed() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(dayCruiseCalendarLocator));
        WebElement dayCruiseCalendar = driver.findElement(dayCruiseCalendarLocator);
        return dayCruiseCalendar.isDisplayed();
    }

    public boolean isCruiseCalendarDisplayed() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(cruiseCalendarLocator));
        WebElement cruiseCalendar = driver.findElement(cruiseCalendarLocator);
        return cruiseCalendar.isDisplayed();
    }

    public boolean isRouteCalendarDisplayed() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(routeCalendarLocator));
        WebElement routeCalendar = driver.findElement(routeCalendarLocator);
        return routeCalendar.isDisplayed();
    }

    public String chooseFirstDepartureTime() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.numberOfElementsToBeMoreThan(selectedDateLocator, 0));
        List<WebElement> departureTimes = driver.findElement(selectedDateLocator).findElements(By.className("departure"));
        String departureTime =
                departureTimes.get(0).findElement(By.className("departsAt")).findElement(By.className("time")).getAttribute("data-time");
        departureTimes.get(0).click();
        return departureTime;
    }
}
