package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Header {

    private final WebDriver driver;

    By selectedLanguageLocator = By.cssSelector("#langmenu_titletext > span.ddTitleText");
    By languageMenuArrowLocator = By.id("langmenu_arrow");

    public Header(WebDriver driver) {
        this.driver = driver;
    }

    public String selectedLanguage() {
        WebElement selectedLanguage = driver.findElement(selectedLanguageLocator);
        return selectedLanguage.getText();
    }

    public void selectLanguage(String language) {
        WebElement languageArrow = driver.findElement(languageMenuArrowLocator);
        languageArrow.click();

        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//div[@id='langmenu_child']/a[contains(., '" + language + "')]")));
        WebElement selection = driver.findElement(
                By.xpath("//div[@id='langmenu_child']/a[contains(., '" + language + "')]"));
        ((JavascriptExecutor)driver).executeScript("document.getElementById('" + selection.getAttribute("id") + "').click()");

        /*By languageSelectorStr = By.cssSelector("a.enabled.langmenu > span:contains('" + language + "')");
        WebElement languageButton = driver.findElement(languageSelectorStr);
        languageButton.click();*/
    }
}
