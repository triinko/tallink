package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VoyageType {

    private final WebDriver driver;

    By shuttleLocator = By.xpath("//li[@data-code='SHUTTLE']");
    By dayCruiseLocator = By.xpath("//li[@data-code='DAYCRUISE']");
    By cruiseLocator = By.xpath("//li[@data-code='CRUISE']");
    By routeTripLocator = By.xpath("//li[@data-code='ROUTETRIP']");
    By hotelLocator = By.xpath("//li[@data-code='HOTEL']");
    By selectedTypeLocator = By.xpath("//section[@class='voyageType visible']/ul/li[contains(@class, 'chosen')]");

    public VoyageType(WebDriver driver) {
        this.driver = driver;
    }

    public void selectShuttle() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(shuttleLocator));
        WebElement element = driver.findElement(shuttleLocator);
        element.click();
    }

    public void selectDayCruise() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(dayCruiseLocator));
        WebElement element = driver.findElement(dayCruiseLocator);
        element.click();
    }

    public void selectCruise() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(cruiseLocator));
        WebElement element = driver.findElement(cruiseLocator);
        element.click();
    }

    public void selectRouteTrip() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(routeTripLocator));
        WebElement element = driver.findElement(routeTripLocator);
        element.click();
    }

    public void selectHotel() {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(hotelLocator));
        WebElement element = driver.findElement(hotelLocator);
        element.click();
    }

    public String selectedVoyageType() {
        WebElement selectedType = driver.findElement(selectedTypeLocator);
        return selectedType.getAttribute("data-code");
    }
}
