package pageobjects;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Booking {

    private final WebDriver driver;

    By addAdultsButtonLocator = By.cssSelector("div.spinnerWrapper.adults button.increase");
    By addChildrenButtonLocator = By.cssSelector("div.spinnerWrapper.children button.increase");
    By addJuniorsButtonLocator = By.cssSelector("div.spinnerWrapper.juniors button.increase");
    By addYouthsButtonLocator = By.cssSelector("div.spinnerWrapper.youths button.increase");
    By removeAdultsButtonLocator = By.cssSelector("div.spinnerWrapper.adults button.decrease");
    By removeChildrenButtonLocator = By.cssSelector("div.spinnerWrapper.children button.decrease");
    By removeJuniorsButtonLocator = By.cssSelector("div.spinnerWrapper.juniors button.decrease");
    By removeYouthsButtonLocator = By.cssSelector("div.spinnerWrapper.youths button.decrease");
    By numberOfAdultsAddedLocator = By.cssSelector("div.spinnerWrapper.adults input.value");
    By numberOfChildrenAddedLocator = By.cssSelector("div.spinnerWrapper.children input.value");
    By numberOfJuniorsAddedLocator = By.cssSelector("div.spinnerWrapper.juniors input.value");
    By numberOfYouthsAddedLocator = By.cssSelector("div.spinnerWrapper.youths input.value");
    By searchButtonLocator = By.className("searchButton");
    By chosenDestinationLocator = By.cssSelector("label.destinationPortButton.chosen > span");
    By departureMenuArrowLocator = By.id("departurePortMenu_arrow");
    By chosenDepartureLocator = By.cssSelector("#departurePortMenu_titletext > span.ddTitleText");
    By chosenDepartureDateLocator = By.cssSelector("td.date.odd.selected");
    By clubOneButtonLocator = By.className(" clubone");
    By calendarTitleLocator = By.className("calendarTitle");
    By sailOptionsLocator = By.cssSelector("table[class='sailOptions']");
    By entertainmentSectionLocator = By.className("event");

    public Booking(WebDriver driver) {
        this.driver = driver;
    }

    public void clickAddAdultsButton() {
        WebElement increaseButton = driver.findElement(addAdultsButtonLocator);
        increaseButton.click();
    }

    public void clickRemoveAdultsButton() {
        WebElement decreaseButton = driver.findElement(removeAdultsButtonLocator);
        decreaseButton.click();
    }

    public int numberOfAdultsAdded() {
        WebElement numberOfAdultsInput = driver.findElement(numberOfAdultsAddedLocator);
        return Integer.parseInt(numberOfAdultsInput.getAttribute("value"));
    }

    public boolean isAddAdultButtonEnabled() {
        WebElement increaseButton = driver.findElement(addAdultsButtonLocator);
        return increaseButton.isEnabled();
    }

    public void clickAddChildrenButton() {
        WebElement increaseButton = driver.findElement(addChildrenButtonLocator);
        increaseButton.click();
    }

    public void clickRemoveChildrenButton() {
        WebElement decreaseButton = driver.findElement(removeChildrenButtonLocator);
        decreaseButton.click();
    }

    public int numberOfChildrenAdded() {
        WebElement numberOfChildrenInput = driver.findElement(numberOfChildrenAddedLocator);
        return Integer.parseInt(numberOfChildrenInput.getAttribute("value"));
    }

    public void clickAddJuniorsButton() {
        WebElement increaseButton = driver.findElement(addJuniorsButtonLocator);
        increaseButton.click();
    }

    public void clickRemoveJuniorsButton() {
        WebElement decreaseButton = driver.findElement(removeJuniorsButtonLocator);
        decreaseButton.click();
    }

    public int numberOfJuniorsAdded() {
        WebElement numberOfJuniorsInput = driver.findElement(numberOfJuniorsAddedLocator);
        return Integer.parseInt(numberOfJuniorsInput.getAttribute("value"));
    }

    public void clickAddYouthsButton() {
        WebElement increaseButton = driver.findElement(addYouthsButtonLocator);
        increaseButton.click();
    }

    public void clickRemoveYouthsButton() {
        WebElement decreaseButton = driver.findElement(removeYouthsButtonLocator);
        decreaseButton.click();
    }

    public int numberOfYouthsAdded() {
        WebElement numberOfYouthsInput = driver.findElement(numberOfYouthsAddedLocator);
        return Integer.parseInt(numberOfYouthsInput.getAttribute("value"));
    }

    public void clickSearchButton() {
        WebElement searchButton = driver.findElement(searchButtonLocator);
        searchButton.click();
    }

    public void chooseDestination(String destination) {

        WebElement destinationButton = driver.findElement(
                By.xpath("//label[@class='destinationPortButton ']/descendant::span[text()='" + destination + "']"));
        destinationButton.click();
    }

    public String chosenDestination() {

        WebElement chosenDestination = driver.findElement(chosenDestinationLocator);
        return chosenDestination.getText();
    }

    public void chooseDeparture(String departure) {

        WebElement departureArrow = driver.findElement(departureMenuArrowLocator);
        departureArrow.click();

        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//div[@id='departurePortMenu_child']/a[@class='enabled' and contains(., '" + departure + "')]")));
        WebElement selection = driver.findElement(
                By.xpath("//div[@id='departurePortMenu_child']/a[contains(., '" + departure + "')]"));
        ((JavascriptExecutor)driver).executeScript("document.getElementById('" + selection.getAttribute("id") + "').click()");
    }

    public String chosenDeparture() {

        WebElement chosenDeparture = driver.findElement(chosenDepartureLocator);
        return chosenDeparture.getText();
    }

    public void chooseDate(String departureDate) {

        WebElement dateElement = driver.findElement(By.xpath("//td[@date-iso='" + departureDate + "']"));
        dateElement.click();
    }

    public String chosenDepartureDate() {

        WebElement dateElement = driver.findElement(chosenDepartureDateLocator);
        return dateElement.getAttribute("date-iso");
    }

    public void clickClubOne() {

        WebElement element = driver.findElement(clubOneButtonLocator);
        element.click();
    }

    public boolean isClubOnePageSelected() {

        try {
            WebElement element = driver.findElement(By.className("active clubone"));
            return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }

    }

    public String calendarTitle() {

        WebElement element = driver.findElement(calendarTitleLocator);
        return element.getText();
    }
    
    public void chooseVehicle() {
    	driver.findElement(By.cssSelector("input.travellingWithVehicle")).click();
        driver.findElement(By.cssSelector("a.searchButton > span")).click();
    }
    
    public void changeVehicleType() {
    	driver.findElement(By.cssSelector("div.ddTitle > div.arrow")).click();
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        driver.findElement(By.xpath("//div[@id='main']/div/div/section[8]/div/table/tbody/tr[8]/td/div")).click();
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

    /**
     * Helsinki laeva kajut
     */
	public void chooseClass() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//button[@value='B']")).click();
	}

	/**
	 * Stockholmi laeva kajut
	 */
	public void chooseCabin() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.cssSelector("button.button.travelClassOutward")).click();
	}

	public void chooseFood() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.xpath("(//button[@type='button'])[14]")).click();
	}

    public void chooseEntertainment() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(sailOptionsLocator));
        List<WebElement> options  = driver.findElements(entertainmentSectionLocator);
        options.get(0).findElement(By.cssSelector("button[class='button eventTime']")).click();
        WebElement increase = driver.findElement(By.className("ticket")).findElement(By.cssSelector("button[class='increase']"));
        increase.click();

    }

	public void chooseHotel() {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.cssSelector("li.HOTEL > span")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void insertContacts() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.cssSelector("button.pay")).click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.cssSelector("li.active")).click();
		driver.findElement(By.name("first_name")).clear();
	    driver.findElement(By.name("first_name")).sendKeys("asd");
	    driver.findElement(By.name("last_name")).clear();
	    driver.findElement(By.name("last_name")).sendKeys("asd");
	    driver.findElement(By.name("phone")).click();
	    driver.findElement(By.name("phone")).clear();
	    driver.findElement(By.name("phone")).sendKeys("+372  5448 5451");
	    driver.findElement(By.name("email")).clear();
	    driver.findElement(By.name("email")).sendKeys("asd@gmail.com");
	}

	public void clickPay() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.cssSelector("button.pay")).click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.cssSelector("li.active")).click();
    }

	public void insertFirstName(String firstName) {

        driver.findElement(By.name("first_name")).clear();
        driver.findElement(By.name("first_name")).sendKeys(firstName);
    }

    public String firstName() {
        return driver.findElement(By.name("first_name")).getAttribute("value");
    }

    public void insertLastName(String lastName) {
        driver.findElement(By.name("last_name")).clear();
        driver.findElement(By.name("last_name")).sendKeys(lastName);
    }

    public String lastName() {
        return driver.findElement(By.name("last_name")).getAttribute("value");
    }

    public void insertMobileNumber(String mobileNumber) {
        driver.findElement(By.name("phone")).click();
        driver.findElement(By.name("phone")).clear();
        driver.findElement(By.name("phone")).sendKeys(mobileNumber);
    }

    public void insertEmail(String email) {
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys(email);
    }

    public String emailText() {
        return driver.findElement(By.name("email")).getAttribute("value");
    }

    public boolean hasFirstNameError() {
        return driver.findElement(By.name("first_name")).getCssValue("color").equals("rgb(229, 49, 42)");
    }

    public boolean hasLastNameError() {
        return driver.findElement(By.name("last_name")).getCssValue("color").equals("rgb(229, 49, 42)");
    }

    public boolean hasPhoneError() {
        return driver.findElement(By.name("phone")).getCssValue("color").equals("rgb(229, 49, 42)");
    }

    public boolean hasEmailError() {
        return driver.findElement(By.name("email")).getCssValue("color").equals("rgb(229, 49, 42)");
    }
	
	public boolean isPayButtonEnabled() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		WebElement element  = driver.findElement(By.xpath("//button[@class='confirm']"));
		return element.isEnabled();
	}

	public void removeFood() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.cssSelector("button.button.chosen")).click();
	}
}
