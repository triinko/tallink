package unit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import tictactoe.TTTConsoleNonOO2P;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Scanner.class, TTTConsoleNonOO2P.class })
public class TicTacToeTests {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    /**
     * Täis mäng, X võidab
     */
    @Test
    public void testPT01() throws Exception {

        Scanner scanner = PowerMockito.mock(Scanner.class);
        InputStream inputStream = PowerMockito.mock(InputStream.class);
        PowerMockito.whenNew(Scanner.class).withArguments(inputStream)
                .thenReturn(scanner);
        PowerMockito.when(scanner.nextInt())
                .thenAnswer(new Answer<Integer>() {
                    int[] inputs = {1, 1, 1, 2, 2, 1, 2, 2, 3, 1};
                    int counter = 0;
                    public Integer answer(InvocationOnMock invocationOnMock) throws Throwable {
                        return inputs[counter++];
                    }
                });
        TTTConsoleNonOO2P.play(scanner);
        Assert.assertEquals("Player 'X', enter your move (row[1-3] column[1-3]):  X |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'X', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                " X |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                " X | O |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'X', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                " X | O |   \n" +
                "-----------\n" +
                " X |   |   \n" +
                "\n" +
                "'X' won! Bye!\n", outContent.toString());
    }

    /**
     * Täis mäng, O võidab
     */
    @Test
    public void testPT02() throws Exception {

        Scanner scanner = PowerMockito.mock(Scanner.class);
        InputStream inputStream = PowerMockito.mock(InputStream.class);
        PowerMockito.whenNew(Scanner.class).withArguments(inputStream)
                .thenReturn(scanner);
        PowerMockito.when(scanner.nextInt())
                .thenAnswer(new Answer<Integer>() {
                    int[] inputs = {1, 1, 1, 2, 1, 3, 2, 2, 3, 1, 3, 2};
                    int counter = 0;
                    public Integer answer(InvocationOnMock invocationOnMock) throws Throwable {
                        return inputs[counter++];
                    }
                });
        TTTConsoleNonOO2P.play(scanner);
        Assert.assertEquals("Player 'X', enter your move (row[1-3] column[1-3]):  X |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'X', enter your move (row[1-3] column[1-3]):  X | O | X \n" +
                "-----------\n" +
                "   |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O | X \n" +
                "-----------\n" +
                "   | O |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'X', enter your move (row[1-3] column[1-3]):  X | O | X \n" +
                "-----------\n" +
                "   | O |   \n" +
                "-----------\n" +
                " X |   |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O | X \n" +
                "-----------\n" +
                "   | O |   \n" +
                "-----------\n" +
                " X | O |   \n" +
                "\n" +
                "'O' won! Bye!\n", outContent.toString());
    }

    /**
     * Täis mäng, viik
     */
    @Test
    public void testPT03() throws Exception {

        Scanner scanner = PowerMockito.mock(Scanner.class);
        InputStream inputStream = PowerMockito.mock(InputStream.class);
        PowerMockito.whenNew(Scanner.class).withArguments(inputStream)
                .thenReturn(scanner);
        PowerMockito.when(scanner.nextInt())
                .thenAnswer(new Answer<Integer>() {
                    int[] inputs = {1, 1, 1, 2, 2, 1, 2, 2, 3, 2, 3, 1, 1, 3, 2, 3, 3, 3};
                    int counter = 0;
                    public Integer answer(InvocationOnMock invocationOnMock) throws Throwable {
                        return inputs[counter++];
                    }
                });
        TTTConsoleNonOO2P.play(scanner);
        Assert.assertEquals("Player 'X', enter your move (row[1-3] column[1-3]):  X |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'X', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                " X |   |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                " X | O |   \n" +
                "-----------\n" +
                "   |   |   \n" +
                "\n" +
                "Player 'X', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                " X | O |   \n" +
                "-----------\n" +
                "   | X |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O |   \n" +
                "-----------\n" +
                " X | O |   \n" +
                "-----------\n" +
                " O | X |   \n" +
                "\n" +
                "Player 'X', enter your move (row[1-3] column[1-3]):  X | O | X \n" +
                "-----------\n" +
                " X | O |   \n" +
                "-----------\n" +
                " O | X |   \n" +
                "\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]):  X | O | X \n" +
                "-----------\n" +
                " X | O | O \n" +
                "-----------\n" +
                " O | X |   \n" +
                "\n" +
                "Player 'X', enter your move (row[1-3] column[1-3]):  X | O | X \n" +
                "-----------\n" +
                " X | O | O \n" +
                "-----------\n" +
                " O | X | X \n" +
                "\n" +
                "It's a Draw! Bye!\n", outContent.toString());
    }

    /**
     * Vale käik
     */
    @Test
    public void testPT04() throws Exception {

        Scanner scanner = PowerMockito.mock(Scanner.class);
        InputStream inputStream = PowerMockito.mock(InputStream.class);
        PowerMockito.whenNew(Scanner.class).withArguments(inputStream)
                .thenReturn(scanner);
        PowerMockito.when(scanner.nextInt())
                .thenAnswer(new Answer<Integer>() {
                    int[] inputs = {0, 1, 3, 1};
                    int counter = 0;
                    public Integer answer(InvocationOnMock invocationOnMock) throws Throwable {
                        return inputs[counter++];
                    }
                });
        TTTConsoleNonOO2P.initGame();
        TTTConsoleNonOO2P.playerMove(TTTConsoleNonOO2P.NOUGHT, scanner);
        Assert.assertEquals("Player 'O', enter your move (row[1-3] column[1-3]): This move at (0,1) is not valid. Try again...\n" +
                "Player 'O', enter your move (row[1-3] column[1-3]): ", outContent.toString());

    }

    /**
     * Prindi ruut, mida ei eksisteeri
     */
    @Test
    public void testPT05() {

        TTTConsoleNonOO2P.printCell(3);
        Assert.assertEquals("", outContent.toString());
    }

    /**
     * Loo TTTConsoleNonOO2P objekt
     */
    @Test
    public void testPT06() {

        TTTConsoleNonOO2P o = new TTTConsoleNonOO2P();
        Assert.assertNotNull(o);
    }

}
