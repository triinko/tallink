package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;

public class Utils {

    private WebDriver driver;
    private long timeout = 15;

    public Utils(WebDriver driver) {
        this.driver = driver;
    }

    public void waitForNumberOfWindowsToEqual(final int numberOfWindows) {
        new WebDriverWait(driver, timeout) {
        }.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return (driver.getWindowHandles().size() >= numberOfWindows);
            }
        });
    }

    public void switchToWindow(final int windowNumber) {

        waitForNumberOfWindowsToEqual(windowNumber + 1);

        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(windowNumber));
    }
}
