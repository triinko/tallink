package acceptance;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pageobjects.*;
import utils.Utils;

import java.util.Calendar;
import java.util.Random;

import static org.junit.Assert.*;

public class AcceptanceTests {

    WebDriver browser;

    @Before
    public void setup() {
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/drivers/geckodriver.exe");
        browser = new FirefoxDriver();
        browser.get("http://tallink.ee");
    }

    /**
     * Veebilehe keele valimine
     */
    @Test
    public void testTC14() {

        Booking booking = new Booking(browser);

        //booking.clickClubOne();
        //assertTrue(booking.isClubOnePageSelected());

        Header header = new Header(browser);
        header.selectLanguage("RU");
        assertEquals("RU", header.selectedLanguage());

        //assertTrue(booking.isClubOnePageSelected());
        //assertEquals("Дата отъезда", booking.calendarTitle());
    }

    /**
     * Reisi kuupäeva valimine
     */
    @Test
    public void testTC17() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        String departureDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);

        Booking booking = new Booking(browser);

        booking.chooseDate(departureDate);
        assertEquals(departureDate, booking.chosenDepartureDate());

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        BookingSummary bookingSummary = new BookingSummary(browser);
        assertTrue(bookingSummary.selectedSailTime().contains(departureDate));
    }

    /**
     * Reisi väljumiskoha valimine
     */
    @Test
    public void testTC13() {

        Booking booking = new Booking(browser);

        booking.chooseDeparture("Turu");
        assertEquals("Turu", booking.chosenDeparture());

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        BookingSummary bookingSummary = new BookingSummary(browser);
        assertTrue(bookingSummary.tripDetail().toLowerCase().contains("turu"));
    }

    /**
     * Reisi sihtkoha valimine
     */
    @Test
    public void testTC15() {

        Booking booking = new Booking(browser);

        booking.chooseDestination("Stockholm");
        assertEquals("Stockholm", booking.chosenDestination());

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        BookingSummary bookingSummary = new BookingSummary(browser);
        assertTrue(bookingSummary.tripDetail().toLowerCase().contains("stockholm"));
    }

    /**
     * Reisijate arvu valimine
     */
    @Test
    public void testTC16() {

        Booking booking = new Booking(browser);

        booking.clickAddAdultsButton();
        booking.clickAddChildrenButton();
        booking.clickAddJuniorsButton();
        booking.clickAddYouthsButton();

        assertEquals(2, booking.numberOfAdultsAdded());
        assertEquals(1, booking.numberOfChildrenAdded());
        assertEquals(1, booking.numberOfJuniorsAdded());
        assertEquals(1, booking.numberOfYouthsAdded());

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        BookingSummary bookingSummary = new BookingSummary(browser);
        assertEquals(2, bookingSummary.adultPassengersCount());
        assertEquals(3, bookingSummary.underagePassengersCount());
    }
    
    /**
     * Sõiduki valimine
     */
    @Test
    public void testTC19() {
    	Booking booking = new Booking(browser);
    	
    	booking.chooseVehicle();
    	
    	booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);
    	
    	BookingSummary bookingSummary = new BookingSummary(browser);
    	assertTrue(bookingSummary.fullTripDetail().toLowerCase().contains("sõiduauto"));
    }
    
    /**
     * Sõiduki tüübi muutmine.
     */
    @Test
    public void testTC20() {
    	Booking booking = new Booking(browser);
    	
    	booking.chooseVehicle();
    	
    	booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);
    	
    	BookingSummary bookingSummary = new BookingSummary(browser);
    	assertTrue(bookingSummary.fullTripDetail().toLowerCase().contains("sõiduauto"));
    	
    	booking.changeVehicleType();
    	assertTrue(bookingSummary.fullTripDetail().toLowerCase().contains("mootorratas"));
    }
    
    /**
     * Kajuti valimine lühiajalise reisi puhul.
     */
    @Test
    public void testTC25() {
    	Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        String departureDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);

        Booking booking = new Booking(browser);

        booking.chooseDate(departureDate);
        
        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);
        
        booking.chooseClass();
        
        BookingSummary bookingSummary = new BookingSummary(browser);
    	assertTrue(bookingSummary.fullTripDetail().toLowerCase().contains("b-klassi kajut"));
    }
    
    /**
     * Kajuti valimine pikaajalise reisi puhul.
     */
    @Test
    public void testTC23() {
    	Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        String departureDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);

        Booking booking = new Booking(browser);

        booking.chooseDate(departureDate);
        booking.chooseDestination("Stockholm");
        
        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);
        
        booking.chooseCabin();
        
        BookingSummary bookingSummary = new BookingSummary(browser);
    	assertTrue(bookingSummary.fullTripDetail().toLowerCase().contains("b-klassi kajut"));
    }


    /**
     * Reisijate eemaldamine
     */
    @Test
    public void testTC18() {

        Booking booking = new Booking(browser);

        Random random = new Random();
        int adultCount = random.nextInt(5) + 1;
        int childCount = random.nextInt(5) + 1;
        int juniorCount = random.nextInt(5) + 1;
        int youthCount = random.nextInt(5) + 1;

        for(int i = 0; i < adultCount; i++) {
            booking.clickAddAdultsButton();
        }

        for(int i = 0; i < childCount; i++) {
            booking.clickAddChildrenButton();
        }

        for(int i = 0; i < juniorCount; i++) {
            booking.clickAddJuniorsButton();
        }

        for(int i = 0; i < youthCount; i++) {
            booking.clickAddYouthsButton();
        }

        int removedAdultCount = random.nextInt(adultCount) + 1;
        int removedChildCount = random.nextInt(childCount) + 1;
        int removedJuniorCount = random.nextInt(juniorCount) + 1;
        int removedYouthCount = random.nextInt(youthCount) + 1;

        for(int i = 0; i < removedAdultCount; i++) {
            booking.clickRemoveAdultsButton();
        }

        for(int i = 0; i < removedChildCount; i++) {
            booking.clickRemoveChildrenButton();
        }

        for(int i = 0; i < removedJuniorCount; i++) {
            booking.clickRemoveJuniorsButton();
        }

        for(int i = 0; i < removedYouthCount; i++) {
            booking.clickRemoveYouthsButton();
        }

        assertEquals(adultCount - removedAdultCount + 1, booking.numberOfAdultsAdded());
        assertEquals(childCount - removedChildCount, booking.numberOfChildrenAdded());
        assertEquals(juniorCount - removedJuniorCount, booking.numberOfJuniorsAdded());
        assertEquals(youthCount - removedYouthCount, booking.numberOfYouthsAdded());

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        BookingSummary bookingSummary = new BookingSummary(browser);
        assertEquals(adultCount - removedAdultCount + 1, bookingSummary.adultPassengersCount());
        int totalChildren = childCount - removedChildCount + juniorCount - removedJuniorCount + youthCount - removedYouthCount;
        assertEquals(totalChildren, bookingSummary.underagePassengersCount());
    }

    /**
     * Reisi tüübi valimine
     */
    @Test
    public void testTC21() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        VoyageType voyageType = new VoyageType(browser);
        TimeTable timeTable = new TimeTable(browser);

        voyageType.selectDayCruise();
        assertEquals("DAYCRUISE", voyageType.selectedVoyageType());
        assertTrue(timeTable.isDayCruiseCalendarDisplayed());

        voyageType.selectShuttle();
        assertEquals("SHUTTLE", voyageType.selectedVoyageType());
        assertTrue(timeTable.isShuttleCalendarDisplayed());

        booking.chooseDestination("Stockholm");

        voyageType.selectRouteTrip();
        assertEquals("ROUTETRIP", voyageType.selectedVoyageType());
        assertTrue(timeTable.isRouteCalendarDisplayed());

        voyageType.selectCruise();
        assertEquals("CRUISE", voyageType.selectedVoyageType());
        assertTrue(timeTable.isCruiseCalendarDisplayed());
    }
    
    /**
     * Toitlustuse tellimine.
     */
    @Test
    public void testTC24() {
    	Booking booking = new Booking(browser);

        booking.chooseDestination("Stockholm");
        
        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);
        
        booking.chooseCabin();
        booking.chooseFood();
        
        BookingSummary bookingSummary = new BookingSummary(browser);
        assertTrue(bookingSummary.fullTripDetail().toLowerCase().contains("hommikusöök"));
    }
    
    /**
     * Hotellitoa broneerimine.
     */
    @Test
    public void testTC26() {
    	Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        String departureDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);

        Booking booking = new Booking(browser);

        booking.chooseDate(departureDate);
        
        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);
        
        booking.chooseHotel();
        
        BookingSummary bookingSummary = new BookingSummary(browser);
        assertTrue(bookingSummary.hotelTripDetail().toLowerCase().contains("sisseregistreerimine"));
    }

    /**
     * Reisi väljumisaja valimine
     */
    @Test
    public void testTC22() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        String departureDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);

        Booking booking = new Booking(browser);

        booking.chooseDate(departureDate);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        TimeTable timeTable = new TimeTable(browser);
        String departureTime = timeTable.chooseFirstDepartureTime();

        BookingSummary bookingSummary = new BookingSummary(browser);

        assertTrue(bookingSummary.selectedSailTime().contains(departureTime));
    }
    
    /**
     * Teenuse muutudes toimub uue hinna arvutamine
     */
    @Test
    public void testTC28() {
    	Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        String departureDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);

        Booking booking = new Booking(browser);

        booking.chooseDate(departureDate);
        
        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);
        
        //get old price:
        BookingSummary bookingSummary = new BookingSummary(browser);
        int initialPrice = bookingSummary.tripPriceAmount();
        
        booking.chooseClass();
        
        //get new price and compare not equals:
        int updatedPrice = bookingSummary.tripPriceAmount();
        assertTrue(initialPrice != updatedPrice);
    }
    
    /**
     * Maksmine
     */
    @Test
    public void TestTC29() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        String departureDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.insertContacts();

        assertEquals(true, booking.isPayButtonEnabled());
    }

    /**
     * Meelelahutuse valimine
     */
    @Test
    public void testTC27() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        String departureDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);

        Booking booking = new Booking(browser);

        booking.chooseDate(departureDate);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.chooseEntertainment();

        BookingSummary bookingSummary = new BookingSummary(browser);
        assertTrue(bookingSummary.hasLandServices());
    }
    
    /**
     * Toitlustuse tellimusest eemaldamine
     */
    @Test
    public void testTC30() {
    	Booking booking = new Booking(browser);

        booking.chooseDestination("Stockholm");
        
        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);
        
        booking.chooseCabin();
        booking.chooseFood();
        
        BookingSummary bookingSummary = new BookingSummary(browser);
        assertTrue(bookingSummary.fullTripDetail().toLowerCase().contains("hommikusöök"));
        
        booking.removeFood();
        
        assertTrue(!bookingSummary.fullTripDetail().toLowerCase().contains("hommikusöök"));
    }

    @After
    public void teardown() {
        browser.quit();
    }
}
