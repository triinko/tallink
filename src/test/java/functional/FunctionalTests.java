package functional;

import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pageobjects.Booking;
import utils.Utils;

import static org.junit.Assert.assertEquals;


public class FunctionalTests {

    WebDriver browser;

    @Before
    public void setup() {
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/drivers/geckodriver");
        browser = new FirefoxDriver();
        browser.get("http://tallink.ee");
    }

    @After
    public void teardown() {
        browser.quit();
    }

    @Test
    public void testFTC1() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(true, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC2() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("AAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        booking.insertLastName("BBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
        booking.insertMobileNumber("+123456789023456789");
        String email = StringUtils.leftPad("@b.ee", 249, 'a');
        booking.insertEmail(email);

        assertEquals(true, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC3() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A-A");
        booking.insertLastName("B-B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(true, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC4() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("3");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(true, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC5() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC6() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        String name = StringUtils.repeat("A", 30);
        booking.insertFirstName(name);
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(name.substring(0, name.length()-1), booking.firstName());
        assertEquals(true, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC7() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("-A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(true, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC8() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A-");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(true, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC9() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A--B");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(true, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC10() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("4");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(true, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC11() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC12() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        String name = StringUtils.repeat("B", 30);
        booking.insertLastName(name);
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(name.substring(0, name.length()-1), booking.lastName());
        assertEquals(true, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC13() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("-B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(true, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC14() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B-");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(true, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC15() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B--B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(true, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC16() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("1234");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(true, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC17() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+12");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(true, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC18() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+12a");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(true, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC19() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+" + StringUtils.repeat("9", 19));
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(true, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC20() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("aab.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(true, booking.hasEmailError());
    }

    @Test
    public void testFTC21() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC22() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        String email = StringUtils.leftPad("@b.ee", 250, 'a');
        booking.insertEmail(email);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(email.substring(0, email.length()-1), booking.emailText());
        assertEquals(true, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC23() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@baee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(true, booking.hasEmailError());
    }

    @Test
    public void testFTC24() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(true, booking.hasEmailError());
    }

    @Test
    public void testFTC25() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b..ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(true, booking.hasEmailError());
    }

    @Test
    public void testFTC26() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("?");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(true, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC27() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("?");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(true, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(false, booking.hasEmailError());
    }

    @Test
    public void testFTC28() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@?.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(true, booking.hasEmailError());
    }

    @Test
    public void testFTC29() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail(".a@b.ee");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(true, booking.hasEmailError());
    }

    @Test
    public void testFTC30() {

        Booking booking = new Booking(browser);

        booking.clickSearchButton();

        Utils utils = new Utils(browser);
        utils.switchToWindow(1);

        booking.clickPay();
        booking.insertFirstName("A");
        booking.insertLastName("B");
        booking.insertMobileNumber("+123");
        booking.insertEmail("a@b.ee.");

        assertEquals(false, booking.isPayButtonEnabled());
        assertEquals(false, booking.hasFirstNameError());
        assertEquals(false, booking.hasLastNameError());
        assertEquals(false, booking.hasPhoneError());
        assertEquals(true, booking.hasEmailError());
    }
}
